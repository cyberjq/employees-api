import logging

from src.config import config
from src.main import app

import uvicorn

if __name__ == "__main__":
    log_level = logging.INFO if config.ENVIRONMENT.is_debug else logging.WARNING
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level=log_level, workers=1)
