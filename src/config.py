from typing import Any, Union

from pydantic import BaseSettings
from pydantic.networks import MongoDsn

from src.constants import Environment


class Config(BaseSettings):
    MONGO_URI: MongoDsn
    ENVIRONMENT: Environment = Environment.PRODUCTION

    CORS_ORIGINS: list[str]
    CORS_ORIGINS_REGEX: Union[str, None]
    CORS_HEADERS: list[str]

    class Config:
        env_file = "./.env"


config = Config()

app_configs: dict[str, Any] = {"title": "Employees API"}

if not config.ENVIRONMENT.is_debug:
    app_configs["openapi_url"] = None

if config.ENVIRONMENT.is_debug:
    app_configs["debug"] = True
