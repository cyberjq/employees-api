import math

from fastapi import APIRouter, Depends

from src.database.repository import ABCRepository
from src.employees import service
from src.employees.constants import EMPLOYEES_RESPONSES
from src.employees.dependencies import get_common_params, get_employees_filtering_params, get_employees_repository
from src.employees.schemas import CommonParams, EmployeesFieldsForFiltering, PageEmployeesResponse

router = APIRouter(prefix="/api/v1")


@router.get("/employees/", response_model=PageEmployeesResponse, responses=EMPLOYEES_RESPONSES)
async def get_employees(
        employees_filtering_params: EmployeesFieldsForFiltering = Depends(get_employees_filtering_params),
        common_params: CommonParams = Depends(get_common_params),
        repository: ABCRepository = Depends(get_employees_repository),
):
    skip = common_params.size * (common_params.page - 1)
    employees = await service.get_employees(repository, employees_filtering_params,
                                            skip=skip, limit=common_params.size,
                                            )
    count_employees = await service.get_count_employees(repository)

    return {
        "page": common_params.page,
        "size": common_params.size,
        "total": math.ceil(count_employees / common_params.size),
        "employees": employees,
    }
