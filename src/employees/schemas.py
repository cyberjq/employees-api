from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class CommonParams(BaseModel):
    size: int = 100
    page: int = 0


class EmployeesFieldsForFiltering(BaseModel):
    name: Optional[str] = None
    email: Optional[str] = None
    age_begin: Optional[int] = None
    age_end: Optional[int] = None
    company: Optional[str] = None
    join_date_begin: Optional[datetime] = None
    join_date_end: Optional[datetime] = None
    job_title: Optional[str] = None
    gender: Optional[str] = None
    salary_begin: Optional[int] = None
    salary_end: Optional[int] = None


class EmployeesResponse(BaseModel):
    name: str
    email: str
    age: int
    company: str
    join_date: datetime
    job_title: str
    gender: str
    salary: int


class PageEmployeesResponse(BaseModel):
    page: int
    size: int
    total: int
    employees: list[EmployeesResponse]
