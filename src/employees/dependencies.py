from datetime import datetime

from fastapi import Query

from src.database.repository import ABCRepository, EmployeesRepository
from src.employees.schemas import CommonParams, EmployeesFieldsForFiltering

DEFAULT_EMPLOYEES_REPOSITORY = EmployeesRepository()


async def get_employees_repository() -> ABCRepository:
    return DEFAULT_EMPLOYEES_REPOSITORY


async def get_common_params(
        page: int = Query(1, gt=0, description="Номер страницы"),
        size: int = Query(100, gt=0, le=1000, description="Количество записей на странице"),
) -> CommonParams:

    return CommonParams(page=page, size=size)


async def get_employees_filtering_params(
        name: str = Query(None, description="ФИО сотрудника"),
        email: str = Query(None, description="Email сотрудника"),
        age_begin: int = Query(None, ge=0, description="Начальный возраст для поиска (включительно)"),
        age_end: int = Query(None, ge=0, description="Конечный возраст для поиска (включительно)"),
        company: str = Query(None, description="Название компании"),
        join_date_begin: datetime = Query(None, description="Начальная дата устройства (включительно)"),
        join_date_end: datetime = Query(None, description="Конечная дата устройства (включительно)"),
        job_title: str = Query(None, description="Название должности сотрудника"),
        gender: str = Query(None, description="Пол сотрудника"),
        salary_begin: int = Query(None, ge=0, description="Начальная зарплата для поиска (включительно)"),
        salary_end: int = Query(None, ge=0, description="Конечная зарплата для поиска (включительно)"),
) -> EmployeesFieldsForFiltering:

    return EmployeesFieldsForFiltering(
        name=name,
        email=email,
        age_begin=age_begin,
        age_end=age_end,
        company=company,
        join_date_begin=join_date_begin,
        join_date_end=join_date_end,
        job_title=job_title,
        gender=gender,
        salary_begin=salary_begin,
        salary_end=salary_end,
    )
