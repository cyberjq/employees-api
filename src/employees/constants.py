
EMPLOYEES_RESPONSES = {
    200: {
        "content": {
            "application/json": {
                "example":
                    [
                        {
                            "name": "Name Name",
                            "email": "email@email.com",
                            "age": 40,
                            "company": "Plarin",
                            "join_date": "2008-11-20T21:47:16",
                            "job_title": "developer",
                            "gender": "male",
                            "salary": 1234,
                        },
                    ],
            },
        },
        "description": "Возвращается список сотрудников с удовлетворяющими фильтрами",
    },
    404: {
        "content": {
            "application/json": {
                "example":
                    {"detail": "employees not found"},
            },
        },
        "description": "Не удалось найти сотрудников",
    },
}


class ErrorMessage:
    EMPLOYEES_NOT_FOUND = "employees not found"
