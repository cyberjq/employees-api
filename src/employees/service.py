from src.database.repository import ABCRepository
from src.employees.exceptions import EmployeesNotFound
from src.employees.schemas import EmployeesFieldsForFiltering


async def get_employees(
        repository: ABCRepository, employees_filter: EmployeesFieldsForFiltering,
        skip: int, limit: int) -> list[dict]:

    employees = await repository.get_employees(employees_filter, skip=skip, limit=limit)

    if not employees:
        raise EmployeesNotFound()

    return employees


async def get_count_employees(repository: ABCRepository) -> int:
    return await repository.get_count_employees()
