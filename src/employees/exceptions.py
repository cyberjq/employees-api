from src.employees.constants import ErrorMessage
from src.exceptions import NotFound


class EmployeesNotFound(NotFound):
    DETAIL = ErrorMessage.EMPLOYEES_NOT_FOUND
