from motor.motor_asyncio import AsyncIOMotorClient

from src.config import config


client = AsyncIOMotorClient(config.MONGO_URI)
employees_database = client["employees"]
employees_collection = employees_database["employees"]
