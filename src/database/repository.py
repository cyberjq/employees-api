from abc import ABC, abstractmethod

from motor.motor_asyncio import AsyncIOMotorCollection

from src.database.database import employees_collection
from src.employees.schemas import EmployeesFieldsForFiltering


class ABCRepository(ABC):

    @abstractmethod
    async def get_employees(
            self, employees_filter: EmployeesFieldsForFiltering, skip: int, limit: int,
    ) -> list[dict]:
        raise NotImplementedError

    @abstractmethod
    async def get_count_employees(self) -> int:
        raise NotImplementedError


DEFAULT_EMPLOYEES_COLLECTION = employees_collection


class EmployeesRepository(ABCRepository):

    def __init__(self, employees_collection: AsyncIOMotorCollection = DEFAULT_EMPLOYEES_COLLECTION):
        self._employees_collection = employees_collection

    @staticmethod
    def _get_filter_for_db(employees_filter: EmployeesFieldsForFiltering) -> dict:
        """
        Создание фильтра для бд. Заполняются все поля фильтра, чтобы сработал индекс.
        :param employees_filter: модель полей для создания фильтра.
        :return: фильтр для бд
        """

        filter_for_db = {}
        # {"$exists": True} if need empty fields
        base_filter_for_field = {"$ne": None}

        filter_for_db["gender"] = employees_filter.gender or base_filter_for_field
        filter_for_db["company"] = employees_filter.company or base_filter_for_field
        filter_for_db["job_title"] = employees_filter.job_title or base_filter_for_field
        filter_for_db["name"] = employees_filter.name or base_filter_for_field

        if employees_filter.age_begin or employees_filter.age_end:
            filter_for_db["age"] = {}
            if employees_filter.age_begin:
                filter_for_db["age"]["$gte"] = employees_filter.age_begin

            if employees_filter.age_end:
                filter_for_db["age"]["$lte"] = employees_filter.age_end
        else:
            filter_for_db["age"] = base_filter_for_field

        if employees_filter.salary_begin or employees_filter.salary_end:
            filter_for_db["salary"] = {}
            if employees_filter.salary_begin:
                filter_for_db["salary"]["$gte"] = employees_filter.salary_begin

            if employees_filter.salary_end:
                filter_for_db["salary"]["$lte"] = employees_filter.salary_end
        else:
            filter_for_db["salary"] = base_filter_for_field

        if employees_filter.join_date_begin or employees_filter.join_date_end:
            filter_for_db["join_date"] = {}
            if employees_filter.join_date_begin:
                filter_for_db["join_date"]["$gte"] = employees_filter.join_date_begin

            if employees_filter.join_date_end:
                filter_for_db["join_date"]["$lte"] = employees_filter.join_date_end
        else:
            filter_for_db["join_date"] = base_filter_for_field

        filter_for_db["email"] = employees_filter.email or base_filter_for_field

        return filter_for_db

    async def get_employees(
            self, employees_filter: EmployeesFieldsForFiltering, skip: int, limit: int,
    ) -> list[dict]:

        filter_for_db = self._get_filter_for_db(employees_filter) if employees_filter.dict(exclude_none=True) else {}

        cursor = self._employees_collection.find(
            filter_for_db, {"_id": 0}, collation={"locale": 'en', "strength": 2},
        ).skip(skip).limit(limit)

        return await cursor.to_list(length=None)

    async def get_count_employees(self) -> int:
        return await self._employees_collection.count_documents({"_id": {"$ne": None}})
