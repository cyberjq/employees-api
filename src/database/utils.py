import datetime
import json

from motor.motor_asyncio import AsyncIOMotorCollection

import pymongo
from pymongo.collation import CollationStrength

from src.database.database import employees_collection

DEFAULT_EMPLOYEES_COLLECTION = employees_collection

DEFAULT_EMPLOYEES_FILE_PATH = "employees.json"


def _datetime_decoder(data: dict) -> dict:
    data["join_date"] = datetime.datetime.strptime(data["join_date"], "%Y-%m-%dT%H:%M:%S%z")
    return data


def _get_employees(file_path: str = DEFAULT_EMPLOYEES_FILE_PATH) -> dict:
    try:
        with open(file_path, "r") as employees_file:
            return json.load(employees_file, object_hook=_datetime_decoder)
    except FileNotFoundError:
        return {}


async def load_employees_in_db(collection: AsyncIOMotorCollection = DEFAULT_EMPLOYEES_COLLECTION) -> None:
    numbers_employees = await collection.count_documents({"_id": {"$ne": None}})

    if numbers_employees > 0:
        return

    employees = _get_employees()
    if not employees:
        return

    await collection.insert_many(employees)


async def create_indexes(collection: AsyncIOMotorCollection = DEFAULT_EMPLOYEES_COLLECTION) -> None:
    await collection.create_index([
        ("gender", pymongo.ASCENDING),
        ("company", pymongo.ASCENDING),
        ("job_title", pymongo.ASCENDING),
        ("name", pymongo.ASCENDING),
        ("age", pymongo.ASCENDING),
        ("salary", pymongo.ASCENDING),
        ("join_date", pymongo.ASCENDING),
        ("email", pymongo.ASCENDING),
    ],
        collation={"locale": "en", "strength": CollationStrength.SECONDARY},
        name="all_fields_index",
    )
