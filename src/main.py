from contextlib import asynccontextmanager

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi

from src.config import app_configs, config
from src.constants import TAGS_METADATA
from src.database import utils as db_utils
from src.employees.router import router as employees_router

from starlette.middleware.cors import CORSMiddleware


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema

    openapi_schema = get_openapi(
        title="API",
        version="1.0",
        description="АПИ сотрудников",
        routes=app.routes,
        tags=TAGS_METADATA,
    )

    app.openapi_schema = openapi_schema
    return app.openapi_schema


@asynccontextmanager
async def lifespan(*args, **kwargs):
    if config.ENVIRONMENT.is_debug:
        await db_utils.load_employees_in_db()
        await db_utils.create_indexes()
    yield


def create_app() -> FastAPI:
    application = FastAPI(
        lifespan=lifespan,
        **app_configs,
    )
    application.include_router(employees_router, tags=["employees"])

    application.openapi = custom_openapi

    application.add_middleware(
        CORSMiddleware,
        allow_origins=config.CORS_ORIGINS,
        allow_origin_regex=config.CORS_ORIGINS_REGEX,
        allow_credentials=True,
        allow_methods=("GET"),
        allow_headers=config.CORS_HEADERS,
    )

    return application


app = create_app()
