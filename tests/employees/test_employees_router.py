from datetime import datetime

from async_asgi_testclient import TestClient

from fastapi import status

import pytest

"""
Используется реальная база данных. Лучше сделать на тестовой бд и коллекции
или подменить базу данных или подменить работу репозитория
"""


@pytest.mark.asyncio
async def test_employees_found(client: TestClient) -> None:
    response = await client.get(
        "/api/v1/employees",
        query_string={
            "gender": "male",
            "company": "plarin",
            "job_title": "developer",
            "age_begin": 18,
            "age_end": 100,
            "salary_begin": 1,
            "salary_end": 100000,
            "join_date_begin": 0,
            "join_date_end": datetime.utcnow().timestamp(),
            # "name": "vance meyers",
            # "email": "integer.sem.elit@lacinia.ca",
            "size": 100,
            "page": 1,
        },
    )

    resp_json = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert len(resp_json["employees"]) > 0


@pytest.mark.asyncio
async def test_employees_not_found(client: TestClient) -> None:
    response = await client.get(
        "/api/v1/employees",
        query_string={
            "company": "unknown_company",
        },
    )

    resp_json = response.json()

    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert resp_json["detail"] == "employees not found"


@pytest.mark.asyncio
async def test_check_limit(client: TestClient) -> None:
    response = await client.get(
        "/api/v1/employees",
        query_string={
            "gender": "male",
            "company": "plarin",
            "job_title": "developer",
            "size": 1,
            "page": 1,
        },
    )

    resp_json = response.json()

    assert response.status_code == status.HTTP_200_OK
    assert len(resp_json["employees"]) == 1


@pytest.mark.asyncio
async def test_invalid_params(client: TestClient) -> None:
    response = await client.get(
        "/api/v1/employees",
        query_string={
            "size": -1,
            "page": -1,
        },
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
